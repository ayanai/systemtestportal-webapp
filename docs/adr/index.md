# Architectural Decision Log

This log lists the architectural decisions for the SystemTestPortal.

<!-- adrlog -- Regenerate the content by using "adr-log -i". You can install it via "npm install -g adr-log" -->

- [ADR-0000](0000-use-markdown-architectural-decision-records.md) - Use Markdown Architectural Decision Records
- [ADR-0001](0001-use-gorilla-sessions.md) - Use gorilla/sessions for session management
- [ADR-0002](0002-use-negroni.md) - Use negroni as middleware
- [ADR-0003](0003-use-httptreemux.md) - Use httptreemux as router
- [ADR-0004](0004-use-xorm.md) - Use xorm as ORM
- [ADR-0005](0005-use-go-sqlite3.md) - Use go-sqlite3 as sqlite3 driver
- [ADR-0006](0006-use-filesystem-for-protocols.md) - Use filesystem to store protocols.md

<!-- adrlogstop -->

For new ADRs, please use [template.md](template.md) as basis.
More information on MADR is available at <https://adr.github.io/madr/>.
General information about architectural decision records is available at <https://adr.github.io/>.
