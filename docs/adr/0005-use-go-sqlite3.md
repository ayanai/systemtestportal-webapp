# Use go-sqlite3 as sqlite3 driver

* Status: accepted
* Deciders: @SilentTeaCup, @m.haug, @kuleszdl
* Date: 2018-02-20

Technical Story: #109, #223

## Context and Problem Statement

We decided to store some of our data in a relational database. To make STP easier to set up for new users, we wanted to
support an embedded database system.

## Decision Drivers

* The database system must be supported by xorm
* The database system should not require additional set up from the user
* The database system should be implemented in pure go

## Considered Options

* SQLite
* TiDB
* QL

## Decision Outcome

Chosen option: "SQLite", because it was the only database system supported by xorm.

Negative consequences:

* There is only one supported driver for SQLite in go. This driver requires the use of cgo, which lead to issues with
  cross compiling packages for architectures other than ARM.

  We decided to make SQLite support togglable by build tag and continue using our transient implementation by default,
  so we could continue to provide packages for all major architectures. Those packages are built without SQLite support.