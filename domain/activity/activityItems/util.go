/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package activityItems

import (
	"time"

	"github.com/nicksnyder/go-i18n/i18n"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

func getActivityItemBaseTree(lang string) templates.LoadedTemplate {
	T, _ := i18n.Tfunc(lang)

	defaultFuncMap := map[string]interface{}{
		"provideTimeago": toIso8601Time,
		"T":              T,
	}

	return templates.Load().Funcs(defaultFuncMap)
}

func toIso8601Time(time time.Time) string {
	return time.UTC().Format("2006-01-02T15:04:05-0700")
}
