{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
{{$parent := .}}

{{template "modal-delete-confirm" .DeleteTestCase}}

<div class="modal fade" id="revertModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="fa fa-undo"></i> Revert Test Case</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>{{T "Do you really want to revert the test case to this version?" .}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="buttonRevertConfirmed">{{T "Revert" .}}</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="duplicateModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="fa fa-clone"></i> {{T "Name Test Case Duplicate" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
                <div class="modal-body">
                    <p>{{T "This duplicates the current test case up to the currently shown version" .}}.
                        {{T "The test case duplicate needs a unique name" .}}.</p>
                    <input class="form-control" id="inputDuplicateTestCaseName"
                           placeholder="{{T "Enter name for test case duplicate" .}}" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" type="submit" id="buttonDuplicateConfirmed">{{T "Duplicate" .}}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
                </div>
        </div>
    </div>
</div>
<div class="modal fade" id="helpModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{T "Test Case Details" .}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="help-table">
                    <tr>
                        <td colspan="3">
                            {{T "This shows the details of a test case" .}}.
                        </td>
                    </tr>
                    <tr class="h5">
                        <th>{{T "Buttons" .}}</th>
                        <th>{{T "Function" .}}</th>
                        <th><span class="d-none d-sm-inline">{{T "Shortcut" .}}</span></th>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-secondary">
                                <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Back" .}}</span>
                            </button>
                        </td>
                        <td>Get back to the test case list.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>{{T "Backspace" .}}</button></td>
                    </tr>
                {{ if .ProjectPermissions.DeleteCase }}
                    <tr>
                        <td>
                            <button class="btn btn-danger">
                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Delete" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Open a confirm dialog before deleting the current test case" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>Del</button></td>
                    </tr>
                {{ end }}
                {{ if .ProjectPermissions.DuplicateCase }}
                    <tr>
                        <td>
                            <button class="btn btn-primary" formaction="duplicate">
                                <i class="fa fa-clone" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Duplicate" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Duplicate the test case with history up to the currently selected version" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>D</button></td>
                    </tr>
                {{ end }}
                    <tr>
                        <td>
                            <button class="btn btn-primary" formaction="history">
                                <i class="fa fa-history" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "History" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Show a list with all previous versions of this test case" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>H</button></td>
                    </tr>
                {{ if .ProjectPermissions.EditCase }}
                    <tr>
                        <td>
                            <button class="btn btn-primary" formaction="revert">
                                <abbr title="Reverts the test case to the selected version">
                                    <i class="fa fa-undo" aria-hidden="true"></i>
                                    <span class="d-none d-sm-inline">{{T "Revert" .}}</span>
                                </abbr>
                            </button>
                        </td>
                        <td>{{T "Revert the test case to the selected version" .}}.</td>
                        <td><span class="d-none d-sm-inline">-</span></td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn btn-primary" formaction="edit">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Edit" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Edit the current test case" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>E</button></td>
                    </tr>
                {{ end }}
                {{ if and .ProjectPermissions.AssignCase (eq .TestCaseVersion.VersionNr (index .TestCase.TestCaseVersions 0).VersionNr) }}
                    <tr>
                        <td>
                            <button class="btn btn-primary" disabled>
                                <i class="fa fa-user-plus" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Assignees" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Assign this test case as a task to a team member" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>A</button></td>
                    </tr>
                {{ end }}
                {{ if and .ProjectPermissions.Execute (eq .TestCaseVersion.VersionNr (index .TestCase.TestCaseVersions 0).VersionNr) }}
                    <tr>
                        <td>
                            <button class="btn btn-success">
                                <i class="fa fa-play" aria-hidden="true"></i>
                                <span class="d-none d-sm-inline"> {{T "Start" .}}</span>
                            </button>
                        </td>
                        <td>{{T "Start the execution of the current test case" .}}.</td>
                        <td><button class="btn btn-light d-none d-sm-inline border-dark" disabled>{{T "Enter" .}}</button></td>
                    </tr>
                {{ end }}
                </table>
                <span class="mt-3 float-left">{{T "For more information visit our" .}} <a href="http://docs.systemtestportal.org" target="_blank">{{T "documentation" .}}</a>.</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{T "Close" .}}</button>
            </div>
        </div>
    </div>
</div>
{{ template "modal-tester-assignment" . }}
<div class="tab-card card" id="tabTestCases">
    <nav class="navbar navbar-light action-bar p-3">
        <div class="input-group flex-nowrap">
            <button class="btn btn-secondary mr-2" id="buttonBack">
                <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                <span class="d-none d-md-inline"> {{T "Back" .}}</span>
            </button>
        {{ if .ProjectPermissions.DeleteCase }}
            <button class="btn btn-danger mr-2" id="buttonDelete" data-toggle="modal"
                    data-target="#deleteTestCase">
                <i class="fa fa-trash-o" aria-hidden="true"></i>
                <span class="d-none d-md-inline"> {{T "Delete" .}}</span>
            </button>
        {{ end }}
        {{ if .ProjectPermissions.DuplicateCase }}
            <button class="btn btn-primary mr-2" id="buttonDuplicate" formaction="duplicate"
                    data-toggle="modal" data-target="#duplicateModal">
                <i class="fa fa-clone" aria-hidden="true"></i>
                <span class="d-none d-md-inline"> {{T "Duplicate" .}}</span>
            </button>
        {{ end }}
            <button class="btn btn-primary mr-2" id="buttonHistory" formaction="history">
                <i class="fa fa-history" aria-hidden="true"></i>
                <span class="d-none d-md-inline"> {{T "History" .}}</span>
            </button>
        {{ if .ProjectPermissions.EditCase }}
        {{ if ne .TestCaseVersion.VersionNr (index .TestCase.TestCaseVersions 0).VersionNr }}
            <button class="btn btn-primary mr-2" id="buttonRevert" formaction="revert" data-toggle="modal"
                    data-target="#revertModal">
                <abbr title="Reverts the test case to the selected version">
                    <i class="fa fa-undo" aria-hidden="true"></i>
                    <span class="d-none d-md-inline">{{T "Revert" .}}</span>
                </abbr>
            </button>
        {{ else }}
            <button class="btn btn-primary mr-2" id="buttonEdit" formaction="edit">
                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                <span class="d-none d-md-inline"> {{T "Edit" .}}</span>
            </button>
        {{ end }}
        {{ end }}
        {{ if and .ProjectPermissions.AssignCase (eq .TestCaseVersion.VersionNr (index .TestCase.TestCaseVersions 0).VersionNr) }}
            <button class="btn btn-primary mr-2 float-right btn-sm" id="buttonAssign" formaction="assign" data-toggle="modal"
                    data-target="#modal-tester-assignment">
                <i class="fa fa-user-plus" aria-hidden="true"></i>
                <span class="d-none d-md-inline"> {{T "Assignees" .}}</span>
            </button>
        {{ end }}
        {{ if and .ProjectPermissions.Execute (eq .TestCaseVersion.VersionNr (index .TestCase.TestCaseVersions 0).VersionNr) }}
        {{ with .TestCaseVersion }}
        {{ if and .Steps .Versions}}
        <button class="btn btn-success" id="buttonExecute">
        {{ else if .Versions}}
        <button class="btn btn-success" id="buttonExecute" disabled data-toggle="tooltip" data-placement="bottom"
                title="" data-oiginal-title="{{T "This test case has no steps" .}}!">
        {{ else }}
        <button class="btn btn-success" id="buttonExecute" disabled data-toggle="tooltip" data-placement="bottom"
                title="" data-original-title="{{T "This Test Case is not applicable to any system-under-test-versions" .}}!">
        {{ end }}
            <i class="fa fa-play" aria-hidden="true"></i>
            <span class="d-none d-md-inline"> {{T "Start" .}}</span>
        </button>
        {{ end }}
        {{ end }}
        </div>
    </nav>
    <div class="row tab-side-bar-row">
        <div class="col-md-9 p-3">
        {{ if ne .TestCaseVersion.VersionNr (index .TestCase.TestCaseVersions 0).VersionNr }}
            <div class="alert alert-warning alert-dismissible">
                <a class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>{{T "Warning" .}}!</strong> {{T "You are viewing an older version of the test case" .}}
            </div>
        {{ end }}
            <h4 class="mb-3">{{ .TestCase.Name }}</h4>
        {{ with .TestCaseVersion }}
            <div class="form-group">
                <label><strong>{{T "Test Case Description" .}}</strong></label>
                <div class="text-muted">
                {{ with .Description }}
                        {{printMarkdown . }}
                        {{ else }}
                    {{T "No Description" .}}
                {{ end }}
                </div>
            </div>
            <div class="form-group">
                <label><strong>{{T "Test Case Preconditions" .}}</strong></label>
                <ul class="list-group" id="preconditionsList">
                {{ if .Preconditions }}
                {{ range .Preconditions }}
                    <li class="list-group-item preconditionItem">
                        <span>{{ .Content }}</span>
                    </li>
                {{ end }}
                {{ else }}
                    <li class="list-group-item">
                        <span>{{T "No preconditions." .}}</span>
                    </li>
                {{ end }}
                </ul>
            </div>
            <div class="form-group">

                <div class="d-flex">
                    <label class="mt-2 mb-2"><strong>{{T "Test Steps" .}}</strong></label>
                    <button id="expand-collapse-all-case-steps" type="button" class="btn btn-link show ml-auto" style="margin-right: .4rem;">
                        <i class="fa fa-angle-double-down" aria-hidden="true" style="font-size: 2em"></i>
                    </button>
                </div>

        {{ if .Steps }}
            <div id="accordion">
            {{ $caseVersion := $.TestCaseVersion }}
            {{ range $index, $step := .Steps }}
                <div class="card">
                    <div class="card-header d-flex flex-row" id="heading-{{$index}}">

                        <span id="stepNumber{{$index}}"></span>

                        <div class="pl-2">
                        {{ printMarkdown (index $caseVersion.Steps $index).Action }}
                        </div>

                        <div style="flex: 1;">
                            <button class="btn btn-link pull-right p-0 collapseElementButton collapsed mouse-hover-pointer" data-toggle="collapse" data-target="#collapse-{{$index}}" aria-expanded="false" aria-controls="collapse-{{$index}}">
                                <i class="fa" aria-hidden="true"></i>
                            </button>
                        </div>
                    </div>

                        <div id="collapse-{{$index}}" class="panel-collapse collapse" aria-labelledby="heading-{{$index}}">
                            <div class="card-body">
                                <b><u>{{T "Expected" .}}:</u></b><br>
                            {{printMarkdown (index $caseVersion.Steps $index).ExpectedResult }}
                            </div>
                        </div>
                    </div>
                {{ end }}
                </div>
                </ol>
            {{ else }}
                <p class="text-muted">{{T "No Test Steps" .}}</p>
            {{ end }}
            </div>
        {{template "comments" $parent}}
        </div>

        <div class="col-md-3 p-3 tab-side-bar">
            <div class="form-group">
                <label><strong>{{T "Applicability" .}}</strong></label>
            {{ if .Versions}}
                <div class="form-group">
                    <div class="col-2 col-sm-1 col-md-12 col-lg-12">
                        <label for="inputTestCaseSUTVersions">
                            <strong>{{T "Versions" .}}</strong>
                        </label>
                    </div>
                    <div class="col-10 col-sm-11 col-md-12 col-lg-12">
                        <select class="custom-select mb-2" id="inputTestCaseSUTVersions"></select>
                    </div>
                    <div class="col-2 col-sm-1 col-md-12 col-lg-12">
                        <label for="inputTestCaseSUTVariants">
                            <strong>{{T "Variants" .}}</strong>
                        </label>
                    </div>
                    <div class="form-group col-10 col-sm-11 col-md-12 col-lg-12">
                        <ul class="list-group" id="inputTestCaseSUTVariants">
                        </ul>
                    </div>
                </div>
            {{ else }}
                <div class="form-group">
                    <p class="text-muted">{{T "This Test Case is not applicable to any system-under-test-versions" .}}.
                        {{T "Edit the Test Case and select applicable versions" .}}.</p>
                </div>
            {{ end }}
            </div>
            <div class="form-group">
                <label><strong>{{T "Estimated Test Duration" .}}</strong></label>
                <p class="text-muted">
                {{ if and (eq $.DurationHours 0) (eq $.DurationMin 0) }}
                    {{T "No Test Duration" .}}
                {{ else }}
                {{ if gt $.DurationHours 0 }}
                {{ $.DurationHours }}
                {{ if eq $.DurationHours 1 }} Hour {{ else }} Hours {{ end }}
                {{ end }}
                {{ if gt $.DurationMin 0 }}
                {{ $.DurationMin }}
                {{ if eq $.DurationMin 1 }}
                    Minute
                {{ else }}
                    Minutes
                {{ end }}
                {{ end }}
                {{ end }}
                </p>
            </div>
            <div class="form-group">
                <label><strong>{{T "Number of Test Steps" .}}</strong></label>
                <p class="text-muted">
                {{ len .Steps }}
                </p>
            </div>
        {{ end }}

    <div class="form-group">
        <label><strong>{{T "Labels" .}}</strong></label>
        <span class="editLabels">
            {{if .User}}
                <button type="button" class="btn btn-primary btn-sm mb-2" data-toggle="modal" data-target="#modal-manage-labels" >
                    <i class="fa fa-wrench" aria-hidden="true"></i>
                </button>
                <div class="form-group">
                    <div class="col-2 col-sm-1 col-md-12 col-lg-12">
                        <strong>{{T "Assign" .}}</strong>
                    </div>
                    <div class="col-10 col-sm-11 col-md-12 col-lg-12" style="margin-top:0.5rem">
                        <div class="" style="overflow-y: scroll; max-height: 212px;border: 1px solid #ced4da;border-radius: .25rem;">
                            <div id="assignLabelContainer" class="col-12">
                            {{range .Project.Labels}}
                                <div id="assign-container-item-{{ .Id }}"
                                     class="input-group mb-3"
                                     onclick="onShowLabelClick({{ .Id }})"
                                     style="margin-top: 1rem;margin-bottom: 0rem;cursor: pointer;">
                                    <form class="form-inline">
                                        <div class="form-group">
                                            <div class="input-group-prepend">
                                                <i id="assign-container-icon-{{ .Id }}"
                                                   class="fa input-group-text"
                                                   data-toggle="tooltip"
                                                   data-original-title="Click to assign/unassign"
                                                   style="padding: 0.9rem"></i>
                                            </div>
                                            <span id="assign-container-label-{{ .Id }}"
                                                class="badge badge-primary clickIcon"
                                                data-toggle="tooltip"
                                                data-original-title="{{ .Description }}"
                                                style="background-color: {{ .Color }};
                                                    color: {{.TextColor}};
                                                    line-height: 2">{{ .Name }}</span>
                                        </div>
                                    </form>
                                </div>
                            {{end}}
                            </div>
                        </div>
                    </div>
                </div>
            {{end}}
            </select>
        </span>
        <p id="labelContainer">
        {{ range .TestCase.Labels }}
            <span class="badge badge-primary" data-toggle="tooltip" title="{{ .Description }}"
                  style="background-color: #{{ .Color }};">{{ .Name }}</span>
        {{ else }}
            <span class="text-muted">No Labels</span>
        {{ end }}
            <span id="showContainerText" class="text-muted">{{T "No Labels" .}}</span>
        </p>
    </div>

    <div class="form-group">
        <label><strong>Assignees</strong></label>
        {{ if and .ProjectPermissions.AssignCase (eq .TestCaseVersion.VersionNr (index .TestCase.TestCaseVersions 0).VersionNr) }}
            <button class="btn btn-primary mr-2 btn-sm" id="buttonAssign" formaction="assign" data-toggle="modal"
                    data-target="#modal-tester-assignment">
                <i class="fa fa-user-plus" aria-hidden="true"></i>
            </button>
        {{ end }}
        <div id="testerContainer" class="mt-2">
            {{if .Tasks }}
                <ul class="list-group">
                {{ range .Tasks }}
                    <li class="list-group-item">
                        <a href="/users/{{.Assignee}}"><img src="{{getUserImageFromID .Assignee}}" alt="" class="rounded-circle profile-picture" height="24" width="24"></a>
                        <a href="/users/{{.Assignee}}"><span class="text-muted assignmentAssignee">{{ .Assignee }}</span></a>
                        <span class="text-muted assignmentVersion">{{ .Version }}</span>
                        <span class="text-muted assignmentVariant">{{ .Variant }}</span>
                    </li>
                {{ end }}
                </ul>
            {{ else }}
                <ul>
                </ul>
                <span class="text-muted">{{T "No assigned testers" .}}</span>
            {{ end }}
        </div>
    </div>
    </div>
    </div>
</div>

<!-- Import Scripts here -->
<script src="/static/js/util/common.js"></script>
<script src="/static/js/project/testcases.js"></script>
<script src="/static/js/project/sut-versions-show.js"></script>
<script src="/static/js/project/testprotocols.js"></script>
<script src="/static/js/util/expand-collapse-all.js"></script>
<script src="/static/assets/js/vendor/jquery.validate.min.js"></script>

<!-- Scripts needed for labels -->
<script>
    // Modal mode controls how the manage label modal interacts with its parent. 1 = show view
    modalMode = 1;

    testAssignedLabelIds = [
        {{range .TestCase.Labels }}
            "{{.Id}}",
        {{end}}
    ];

    checkmarkAssignedLabels();
    showAssignedLabels();

    setShowLabelText();
    setShowLabelTextForAssignments();
</script>

<script>
    $("#printerIcon").removeClass("d-none");
    $("#helpIcon").removeClass("d-none");

    assignButtonsTestCase();
    // Listen to changes in the drop down menu of the variants
    setVersionOnChangeListener('#inputTestCaseSUTVersions', '#inputTestCaseSUTVariants');


    // SUT VARIANTS AND VERSIONS

    // Send a request to get the sut variants and versions
    var url = currentURL();

    // If the newID variable is not yet defined
    // use the test-case-id from the url.
    // Else use the saved name. The name of a test-case
    // needs to be saved because after editing the
    // name of a test case, the url still contains the
    // old name of the test case and the request
    // for the sut-versions would be sent to the wrong url
    if (!newID) {
        newID = url.segments[4];
    }
    if (newID !== {{ .TestCase.ID.Test }}) {
        newID = {{ .TestCase.ID.Test }};
    }


    // Request test case to get the sut versions from it
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", url.takeFirstSegments(4).appendSegment(newID).appendSegment("json").toString(), true);
    xmlhttp.send();
    xmlhttp.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            testcase = JSON.parse(this.responseText);
            testCaseVersionIndex = (testcase.TestCaseVersions).length - {{ .TestCaseVersion.VersionNr }};
            testObjectVersionData = testcase.TestCaseVersions[testCaseVersionIndex].Versions;

            // Fill the drop down menu with the variants
            fillVersions(testObjectVersionData, "#inputTestCaseSUTVersions");
            // Update the version list with the versions of the selected variant
            updateVariantShowList('#inputTestCaseSUTVersions', '#inputTestCaseSUTVariants');
        }
    };

    // BUTTON HANDLERS
    // Handler for the revert button of older test case versions
    $("#buttonRevertConfirmed").click(function () {

        $('#revertModal').on('hidden.bs.modal', function (e) {
            e.preventDefault();

            /* Send the data using post with element ids*/
            var posting = $.ajax({
                url: currentURL().appendSegment("update").toString() + "?fragment=true",
                type: "PUT",
                data: {
                    version: "{{ .TestCaseVersion.VersionNr }}",
                    inputCommitMessage: "Revert test case to version {{ .TestCaseVersion.VersionNr }}",
                    inputTestCaseName: "{{ .TestCase.Name }}",
                    inputTestCaseDescription: "{{ .TestCaseVersion.Description }}",
                    inputTestCasePreconditions: "{{ .TestCaseVersion.Preconditions }}",
                    inputHours: "{{ .DurationHours }}",
                    inputMinutes: "{{ .DurationMin }}"
                }
            });

            /* Alerts the results */
            posting.done(function (response) {
                var url = currentURL()
                        .takeFirstSegments(4)
                        .appendCodedSegment(posting.getResponseHeader("newName"))
                        .toString();

                $('#tabarea').empty().append(response);
                history.pushState('data', '', url);
            }).fail(function (response) {
                $("#modalPlaceholder").empty().append(response.responseText);
                $('#errorModal').modal('show');
            });
        }).modal('hide');
    });

    $('#buttonDuplicateConfirmed').off('click').click(function () {

        $('#duplicateModal').off('click').one('hidden.bs.modal', function (e) {
            e.preventDefault();

            var url = currentURL().appendSegment("duplicate").toString() + "?fragment=true";

            var posting = $.post(url, {
                inputTestCaseName: $('#inputDuplicateTestCaseName').val(),
                version: "{{ .TestCaseVersion.VersionNr }}"
            });

            posting.done(function (r) {
                url = currentURL()
                        .takeFirstSegments(4)
                        .appendCodedSegment(posting.getResponseHeader("newName"))
                        .toString();

                $('#tabTestCases').empty().append(r);
                history.pushState('data', '', url);
            }).fail(function (r) {
                $("#modalPlaceholder").empty().append(r.responseText);
                $('#errorModal').modal('show');
            });
        }).modal('hide');
    });


    $("#printerIcon").removeClass("d-none");

    function printer() {
        window.open(currentURL().appendSegment("print").toString(),
                'newwindow',
                'width=600,height=800');
        return false;
    }
</script>

<!-- Initialize bootstrap-multiselect-->
<script type="text/javascript">

    // testCaseLabels is an array with all the labels of the test case
    var testCaseLabels = {{ .TestCase.Labels }};
    var testSteps = {{ .TestCaseVersion.Steps }} || 0;

    setTestStepNumber(testSteps);
    initExpandCollapseAll("expand-collapse-all-case-steps", "collapseElementButton");

    if (testCaseLabels === null) {
        testCaseLabels = [];
    }

    // Save the labels of project
    // It's needed to reference the correct label when changing the
    // labels of the test case
    var projectLabels = {{ .Project.Labels }};

    $(document).ready(function () {
        var btnEditLabels = $('#btnEditLabels');

        btnEditLabels.multiselect({
            buttonClass: 'btn btn-link',
            buttonText: function (options, select) {
                return 'Edit';
            },
            // Send request to server to update labels of test case
            onDropdownHide: function (event) {
                // The url to send the update request to
                var urlLabelsPost = getTestURL().appendSegment("labels").toString();

                var xhr = new XMLHttpRequest();
                xhr.open("POST", urlLabelsPost, true);
                xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

                // Clear the array with the old labels of the test case
                testCaseLabels.length = 0;
                // Save the newly selected labels in the array with labels
                btnEditLabels.val().forEach(function (labelIndex) {
                    testCaseLabels.push(projectLabels[labelIndex])
                });

                // Send the array of labels as JSON to the server
                xhr.send(JSON.stringify(testCaseLabels));

                xhr.onloadend = function () {
                    if (this.status !== 200) {
                        console.log(this.response);
                    }
                };
                // Update labels of test case in gui
                showLabels(testCaseLabels, "#labelContainer");
            }
        });
        btnEditLabels.multiselect('select', getIndexOfTestCaseLabels());
    });

    // getIndexOfTestCaseLabels returns the indices of the
    // labels of the test case. These labels are selected in
    // the labels-dropdown.
    function getIndexOfTestCaseLabels() {
        var indexList = [];
        testCaseLabels.forEach(function (testCaseLabel) {
            projectLabels.forEach(function (projectLabel, projectLabelIndex) {
                if (testCaseLabel.Name === projectLabel.Name) {
                    indexList.push(projectLabelIndex);
                }
            });
        });
        return indexList;
    }

    setTestStepNumber({{.Steps}});
</script>
{{ end }}