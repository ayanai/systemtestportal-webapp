-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
--
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
ALTER TABLE project_labels ADD COLUMN color VARCHAR(6);

-- +migrate Down
ALTER TABLE project_labels RENAME TO project_labels_old;
CREATE TABLE project_labels (
    id INTEGER PRIMARY KEY,
    project_id INTEGER NOT NULL REFERENCES projects(id) ON DELETE CASCADE,
    name VARCHAR(255),
    description VARCHAR(255),
    CONSTRAINT uniq_project_id_name UNIQUE (project_id, name)
);
INSERT INTO project_labels SELECT id, project_id, name, description FROM project_labels_old;
DROP TABLE project_labels_old;