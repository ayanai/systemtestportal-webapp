/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package duplication

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/modal"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// CasePost handles requests for duplicating testcases.
func CasePost(ta handler.TestCaseAdder, caseChecker id.TestExistenceChecker) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil || c.Case == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DuplicateCase {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		dupTc := *c.Case
		newName := r.FormValue(httputil.TestCaseName)
		if newName != c.Case.Name {
			dupTc.Rename(newName)
		}

		newVersion := dupTc.TestCaseVersions[0]
		newVersion.IsMinor = false
		newVersion.Message = "Duplicated from \"" + c.Case.Name + "\""
		newVersion.VersionNr = len(dupTc.TestCaseVersions) + 1
		dupTc.TestCaseVersions = append([]test.CaseVersion{newVersion}, dupTc.TestCaseVersions...)
		for i := range dupTc.TestCaseVersions {
			dupTc.TestCaseVersions[i].Case = dupTc.ID()
		}

		if err := dupTc.ID().Validate(caseChecker); err != nil {
			errors.Handle(err, w, r)
			return
		}

		if err := cutNewerVersions(&dupTc, getFormValueVersion(r)+1); err != nil {
			errors.Handle(err, w, r)
			return
		}

		err := ta.Add(&dupTc)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		httputil.SetHeaderValue(w, httputil.NewName, dupTc.ItemName())
		w.WriteHeader(http.StatusCreated)

		ctx := context.New().
			WithUserInformation(r).
			With(context.Project, c.Project).
			With(context.TestCase, dupTc).
			With(context.TestCaseVersion, newVersion).
			With(context.Tasks, nil).
			With(context.DurationHours, int(newVersion.Duration.Hours())).
			With(context.DurationMin, newVersion.Duration.GetMinuteInHour()).
			With(context.DurationSec, newVersion.Duration.GetSecondInMinute()).
			With(context.Comments, nil).
			With(context.DeleteTestCase, modal.TestCaseDeleteMessage)

		handler.PrintTmpl(ctx, display.GetTabTestCaseShow(r), w, r)
	}
}

// cutNewerVersions cuts away the versions that are more recent than
// the given version (to). If the version (to) was outside of the
// possible range an error is returned.
func cutNewerVersions(tc *test.Case, to int) error {
	latest := len(tc.TestCaseVersions)
	ver := to
	if ver <= 0 || ver > latest {
		return handler.InvalidTCVersion()
	}
	// Versions are listed backwards
	verIndex := latest - ver
	// Copy current testcase versions and slice afterwards
	tcv := tc.TestCaseVersions
	tc.TestCaseVersions = tcv[verIndex:]
	return nil
}

// getFormValueVersion gets the version from the request.
// Returns 0 if the version is empty.
// Returns -1 if the version is not an integer.
func getFormValueVersion(r *http.Request) int {
	return handler.GetVersion(r, 0)
}
