/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
nse, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the Lice
You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package activityItems

import (
	"bytes"
	"html/template"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

type Assignment struct {
	// Automated Xorm Fields -----------|
	Id        int64
	UpdatedAt time.Time `xorm:"updated"`
	// Foreign Keys --------------------|
	TestId int64
	// ---------------------------------|

	IsCase bool

	Assignees    []*user.User   `xorm:"-"`
	TestCase     *test.Case     `xorm:"-"`
	TestSequence *test.Sequence `xorm:"-"`
}

/*
This struct models the many2many relationship between the Assignment and its Assignees
*/
type AssignedUsers struct {
	AssignmentId int64
	AssigneeId   string
}

func (assignment Assignment) RenderItemTemplate(activity *activity.Activity, lang string) (template.HTML, error) {
	tmpl := getActivityItemBaseTree(lang).Append(templates.Assignment).Get().Lookup("assignment")

	b := &bytes.Buffer{}
	if err := tmpl.Execute(b, activity); err != nil {
		return "", err
	}

	return template.HTML(b.String()), nil
}
