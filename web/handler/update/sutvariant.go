/*
This file is part of SystemTestPortal.
Copyright (C) 2018  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"net/http"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"encoding/json"
	"regexp"
)

const (
	errWrongVersionOrVariantInputTitle = "Could not update the version or variant."
	errWrongVersionOrVariantInput      = "A version name, an old variant name and new variant name] or " +
		" an old version name and a new version name expected."
	errInvalidVersionsOrVariantsInputTitle = "Could not update the version or variant."
	errInvalidVersionsOrVariantsInput      = "only \\sA-Za-z0-9-_().,:äöüÄÖÜß allowed"
	errCouldNotDecodeVariantsTitle         = "Couldn't update the version or variant."
	errCouldNotDecodeVariants              = "We were unable to decode the change to variants " +
		"send in your request. This ist most likely a bug. If you want please " +
		"contact us via our " + handler.IssueTracker + "."
)

/**
 * updates the name of a version or of a variant depending on the input
 */
func ProjectVariantsPut(ps handler.ProjectSUTVersionUpdater) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)

		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if c.User == nil || !c.Project.GetPermissions(c.User).EditCase {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
		}

		var input = []string{}
		if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
			errors.ConstructStd(http.StatusBadRequest,
				errCouldNotDecodeVariantsTitle, errCouldNotDecodeVariants, r).
				WithLog("Could not update the version or variant.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		var invalidInput = regexp.MustCompile(`[^\sA-Za-z0-9-_().,:äöüÄÖÜß]`)
		if len(input) == 2 {
			/*
			 * updates a project version
			 * input[0] is the old version name
			 * input[1] is the new version name
			 */
			if !invalidInput.MatchString(input[0]) &&
				!invalidInput.MatchString(input[1]) {
				if err := ps.RenameProjectVersion(c.Project, input[0], input[1]); err != nil {
					errors.Handle(err, w, r)
					return
				}
			} else {
				errors.ConstructStd(http.StatusBadRequest,
					errInvalidVersionsOrVariantsInputTitle, errInvalidVersionsOrVariantsInputTitle, r).
					WithLog("Could not update the version or variant.").
					WithStackTrace(1).
					WithRequestDump(r).
					Respond(w)
				return
			}
		} else if len(input) == 3 {
			/*
 			* updates a project variant
 			* input[0] is the version name
			* input[1] is the old variant name
			* input[2] is the new variant name
 			*/
			if !invalidInput.MatchString(input[0]) &&
				!invalidInput.MatchString(input[1]) &&
				!invalidInput.MatchString(input[2]) {
				if err := ps.RenameProjectVariant(c.Project, input[0], input[1], input[2]); err != nil {
					errors.Handle(err, w, r)
					return
				}
			} else {
				errors.ConstructStd(http.StatusBadRequest,
					errInvalidVersionsOrVariantsInputTitle, errInvalidVersionsOrVariantsInput, r).
					WithLog("Could not update the version or variant.").
					WithStackTrace(1).
					WithRequestDump(r).
					Respond(w)
				return
			}
		} else {
			errors.ConstructStd(http.StatusBadRequest,
				errWrongVersionOrVariantInputTitle, errWrongVersionOrVariantInput, r).
				WithLog("Could not update the version or variant.").
				WithStackTrace(1).
				WithRequestDump(r).
				Respond(w)
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}
