/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"encoding/json"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// caseExecutionPrinter prints pages for a testcase execution
type caseExecutionPrinter struct {
}

// printStartPage prints the starting page for a testcase execution.
func (printer *caseExecutionPrinter) printStartPage(w http.ResponseWriter, r *http.Request,
	tc test.Case, tcv test.CaseVersion, progress int, progressTotal int, sequenceProtocol *test.SequenceExecutionProtocol, previous bool) {
	c := handler.GetContextEntities(r)
	if c.Project == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	tmpl := getProjectTabPageByName(r, templates.ExecutionStartPage)
	if sequenceProtocol == nil {
		sequenceProtocol, _ = GetCurrentSequenceProtocol(r)
	}

	// initialize values
	// If the execution is started by a task, the version and variant are set via parameter
	var sutVersion, sutVariant = r.FormValue(keyVersion), r.FormValue(keyVariant)
	var isTaskExecution = false
	if sutVersion != "" && sutVariant != "" {
		isTaskExecution = true
	}
	var time = &duration.Duration{}
	var isInSequenceExecution = false
	var isAnonymous = false
	// retrieve pre-set values if in sequence execution
	if sequenceProtocol != nil {
		sutVersion = sequenceProtocol.SUTVersion
		sutVariant = sequenceProtocol.SUTVariant
		err := json.Unmarshal([]byte(r.FormValue(keyDuration)), &time)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		isInSequenceExecution = true
		// If sequence-execution is anonymous, set the anonymous option
		// in the case-start-page corresponding
		isAnonymous = sequenceProtocol.IsAnonymous
	}

	if time == nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToLoadTime, r).
			WithLog("Error while trying to get needed time.").
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
		return
	}

	if previous {
		err := json.Unmarshal([]byte(r.FormValue(keyDuration)), &time)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
	}

	progressPercent := int(float64(progress) / float64(progressTotal) * 100.0)
	caseNr := getFormValueInt(r, keyCaseNr)
	if isInSequenceExecution && !previous {
		caseNr++
	}

	var caseProtocols []test.CaseExecutionProtocol
	caseProtocolsJson := r.FormValue(keyCaseProtocols)

	if caseProtocolsJson != "" {
		err := json.Unmarshal([]byte(caseProtocolsJson), &caseProtocols)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
	}

	var caseProt test.CaseExecutionProtocol
	if caseProtocols == nil || len(caseProtocols) < caseNr {
		caseProt = prepareCaseProtocol(c, tcv)
	} else {
		caseProt = caseProtocols[caseNr-1]
	}

	ctx := context.New().
		WithUserInformation(r).
		With(keyIsSeq, false).
		With(keyIsInSeq, isInSequenceExecution).
		With(keyCaseNr, caseNr).
		With(keyProject, c.Project).
		With(keyTestObject, tc).
		With(keyTestObjectVersion, tcv).
		With(keyEstimatedMinutes, tcv.Duration.GetMinuteInHour()).
		With(keyEstimatedHours, int(tcv.Duration.Hours())).
		With(keyProgress, progress).
		With(keyProgressTotal, progressTotal).
		With(keyProgressPercent, progressPercent).
		With(keySUTVersion, sutVersion).
		With(keySUTVariant, sutVariant).
		With(keyHours, int(time.Hours())).
		With(keyMinutes, time.GetMinuteInHour()).
		With(keySeconds, time.GetSecondInMinute()).
		With(keyIsAnonymous, isAnonymous).
		With(keyCaseProtocol, caseProt).
		With(keySeqProtocol, sequenceProtocol).
		With(keyIsTaskExecution, isTaskExecution)

	handler.PrintTmpl(ctx, tmpl, w, r)
}

// printStepPage prints the page for teststep during execution of a testcase.
func (printer *caseExecutionPrinter) printStepPage(w http.ResponseWriter, r *http.Request, tcv test.CaseVersion,
	stepNr int, progress int, progressTotal int, time duration.Duration, previous bool) {
	c := handler.GetContextEntities(r)
	if c.Project == nil || c.Case == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	tmpl := getProjectTabPageByName(r, templates.ExecutionStep)

	if previous {
		progress = progress - 1
	}

	progressPercent := int(float64(progress) / float64(progressTotal) * 100.0)

	caseNr := getFormValueInt(r, keyCaseNr)

	ctx := context.New()

	ctx.WithUserInformation(r).
		With(keyIsSeq, false).
		With(keyIsInSeq, inSequence(r)).
		With(keyProject, c.Project).
		With(keyTestObject, c.Case).
		With(keyTestObjectVersion, tcv).
		With(keyStepNr, stepNr).
		With(keyCaseNr, caseNr).
		With(keyProgress, progress).
		With(keyProgressTotal, progressTotal).
		With(keyProgressPercent, progressPercent).
		With(keyAction, tcv.Steps[stepNr-1].Action).
		With(keyExpectedResult, tcv.Steps[stepNr-1].ExpectedResult).
		With(keyHours, int(time.Hours())).
		With(keyMinutes, time.GetMinuteInHour()).
		With(keySeconds, time.GetSecondInMinute()).
		With(keyStepProtocol, test.StepExecutionProtocol{})

	handler.PrintTmpl(ctx, tmpl, w, r)
}

// printSummaryPage prints the summary page for the execution of a testcase.
func (printer *caseExecutionPrinter) printSummaryPage(w http.ResponseWriter, r *http.Request, tcv test.CaseVersion,
	progress int, progressTotal int, time duration.Duration, previous bool) {
	c := handler.GetContextEntities(r)
	if c.Project == nil || c.Case == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	tmpl := getProjectTabPageByName(r, templates.ExecutionSummary)
	caseNr := getFormValueInt(r, keyCaseNr)
	var data test.CaseExecutionProtocol
	if previous {
		var caseProtocols []test.CaseExecutionProtocol
		caseProtocolsJson := r.FormValue(keyCaseProtocols)
		err := json.Unmarshal([]byte(caseProtocolsJson), &caseProtocols)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		caseNr--
		if caseNr == -1 {
			progress = progressTotal - 2
			tsv, err := handler.GetTestSequenceVersion(r, c.Sequence)
			if err != nil {
				errors.Handle(err, w, r)
				return
			}
			caseNr = len(tsv.Cases)
		}
		data = caseProtocols[caseNr-1]
	} else {
		err := json.Unmarshal([]byte(r.FormValue(keyCaseProtocol)), &data)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
	}
	// construct step results
	type stepSummary struct {
		Action           string
		Result           test.Result
		ObservedBehavior string
		Comment          string
		NeededTime       duration.Duration
	}
	var steps []stepSummary
	for i := 0; i < len(tcv.Steps); i++ {
		steps = append(steps, stepSummary{
			Action:           tcv.Steps[i].Action,
			Result:           data.StepProtocols[i].Result,
			ObservedBehavior: data.StepProtocols[i].ObservedBehavior,
			Comment:          data.StepProtocols[i].Comment,
			NeededTime:       data.StepProtocols[i].NeededTime,
		})
	}
	progressPercent := int(float64(progress) / float64(progressTotal) * 100.0)

	ctx := context.New().
		WithUserInformation(r).
		With(keyIsInSeq, inSequence(r)).
		With(keyIsSeq, false).
		With(keyTestObjectVersion, tcv).
		With(keyProject, c.Project).
		With(keyTestObject, c.Case).
		With(keyAllSubObjects, steps).
		With(keyCaseNr, caseNr).
		With(keyStepNr, len(tcv.Steps)+1).
		With(keyProgress, progress).
		With(keyProgressTotal, progressTotal).
		With(keyProgressPercent, progressPercent).
		With(keyHours, int(time.Hours())).
		With(keyMinutes, time.GetMinuteInHour()).
		With(keySeconds, time.GetSecondInMinute()).
		With(keyCaseProtocol, data)
	handler.PrintTmpl(ctx, tmpl, w, r)
}

func prepareCaseProtocol(c *handler.ContextEntities, tcv test.CaseVersion) test.CaseExecutionProtocol {
	caseProt := test.CaseExecutionProtocol{}

	caseProt.StepProtocols = make([]test.StepExecutionProtocol, 0)

	caseProt.PreconditionResults = make([]test.PreconditionResult, 0)
	for _, prec := range tcv.Preconditions {
		preconditionResult := test.PreconditionResult{
			Precondition: prec,
			Result:       "",
		}
		caseProt.PreconditionResults = append(caseProt.PreconditionResults, preconditionResult)
	}
	caseProt.UserName = c.User.DisplayName
	caseProt.TestVersion = tcv.ID()

	return caseProt
}
