CURRENT_DATE=`date +%s`
COMMIT_DATE=$(git show -s --format=%ct --date=short $(git rev-parse HEAD))

ONE_DAY_IN_SECONDS=86400

echo "Current Unix Timestamp is: " $CURRENT_DATE
echo "The Unix Timestamp of the last commit is: " $COMMIT_DATE

diff=$(($CURRENT_DATE - $COMMIT_DATE))
if [ $diff -gt $ONE_DAY_IN_SECONDS ]; then
    echo "------------------ABORTING------------------------------------------------"
    echo "Project will not be build again, because no new changes where added today!"
    exit 1;
else
    echo "------------------STARTING New Build--------------------------------------"
    echo "New changes have been comitted in the last 24 hours, we will build a new nightly release."   
fi