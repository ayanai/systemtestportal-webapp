/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package store

import (
	"github.com/go-xorm/xorm"
	"github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/activity"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"net/http"
)

type activityItemStore interface {
	Create(session *xorm.Session, activityType int, activityEntities *activity.ActivityEntities, request *http.Request) (int64, error)
	GetLazyLoadedActivityItem(activity *activity.Activity) (activity.ActivityItem, error)
}

type ActivitySQL struct {
	engine *xorm.Engine
}

func (activitySql ActivitySQL) LogActivity(activityType int, entities *activity.ActivityEntities, request *http.Request) error {

	activityItemStore, err := getActivityItemStore(activityType)
	if err != nil {
		return err
	}

	// We need a session because we are doing multiple inserts. If we get an error we must not commit.
	session := engine.NewSession()

	err = session.Begin()
	if err != nil {
		return err
	}

	// We tell that ItemStore to create the item and return its uniqueId
	itemId, err := activityItemStore.Create(session, activityType, entities, request)
	if err != nil {
		err = multierror.Append(err, session.Rollback())
		return err
	}

	// Get the user name / or anonymous
	var requesterName string
	if entities.User != nil {
		requesterName = entities.User.Name
	} else {
		requesterName = "Anonymous"
	}

	// Create the Activity-Parent object
	newActivity := &activity.Activity{ ActivityType:activityType,
		ActivityItemId: itemId,
	    AuthorId: requesterName,
	    ProjectID: entities.Project.Id }

	_, err = session.Insert(newActivity)
	if err != nil {
		err = multierror.Append(err, session.Rollback())
		return err
	}

	return session.Commit()
}

func getActivityItemStore(activityType int) (activityItemStore, error) {
	var activityItemStore activityItemStore

	// Based on what activity we want to add, a difference ItemStore is set
	switch activityType {
	case activity.ASSIGN_CASE :
		activityItemStore = AssignmentSQL{engine}
	case activity.ASSIGN_SEQUENCE :
		activityItemStore = AssignmentSQL{engine}
	case activity.CREATE_CASE :
		activityItemStore = TestCreationSQL{engine}
	case activity.CREATE_SEQUENCE :
		activityItemStore = TestCreationSQL{engine}
	case activity.PROTOCOL_CASE :
		activityItemStore = TestProtocolSQL{engine}
	case activity.PROTOCOL_SEQUENCE :
		activityItemStore = TestProtocolSQL{engine}

	default:
		err := errors.Errorf("%s%v%s", "No activity of type: ", activityType, " exists")
		return nil, err
	}

	return activityItemStore, nil
}

/**
Gets 20 activities from the database.
 */
func (activitySQL ActivitySQL) GetActivitiesForProjectLimitFromOffset(projectId int64, limit int, offset int) ([]*activity.Activity, error) {

	activities := []*activity.Activity{}

	// Get all activities in this project
	err := activitySQL.engine.Where("project_id = ?", projectId).Limit(limit, offset).Desc("created_at").Find(&activities)
	if err != nil {
		return nil, err
	}

	// Lazy Load all of the activities
	for _, activity := range activities {
		err = activitySQL.lazyLoadActivity(activity)
		if err != nil {
			return nil, err
		}
	}

	return activities, nil
}

func (activitySQL ActivitySQL) lazyLoadActivity(activity *activity.Activity) error {
	userStore := GetUserStore()
	activityItemStore, err := getActivityItemStore(activity.ActivityType)
	if err != nil {
		return err
	}

	// Load the Author
	if activity.AuthorId == "Anonymous" {
		activity.Author = &user.User{
			Name: "Anonymous",
			DisplayName: "Anonymous",
		}
	} else {
		author, _,  err := userStore.Get(id.NewActorID(activity.AuthorId))
		if err != nil {
			return err
		}
		activity.Author = author
	}

	// Load the Project
	project := &project.Project{}
	_ , err = activitySQL.engine.Table("projects").Where("id = ?", activity.ProjectID).Get(project)
	if err != nil {
		return err
	}
	activity.Project = project

	// Load the ActivityItem
	activity.ActivityItem, err = activityItemStore.GetLazyLoadedActivityItem(activity)
	if err != nil {
		return err
	}

	return nil
}

