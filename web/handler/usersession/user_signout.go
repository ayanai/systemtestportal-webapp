/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package usersession

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
)

const (
	failedSignOut  = "Sign out failed."
	noSignedInUser = "It seems you tried to sign out " +
		"without being signed in. So don't worry you are signed out already ;) <br>" +
		"You might need to <a href=\"javascript:window.location.reload();\">refresh</a> " +
		"the page in order to visually see that you're signed out."
)

// signout is the handler for signout requests from the client.
type signout struct {
	session Handler
}

// NewSignoutHandler creates a new signout handler that handles signout requests from the client.
func NewSignoutHandler(handler Handler) http.Handler {
	return &signout{handler}
}

const (
	unableToEndSession = "We were unable to end your session. This is our fault. " +
		"We don't know how this happened and are terribly sorry :|"
)

// ServeHTTP Handles sign out requests from the client.
func (l *signout) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	u, err := l.signoutPossible(r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}
	if endErr := l.session.EndFor(w, r, u); endErr != nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSignOut, unableToEndSession, nil).
			WithLogf("Session can't be ended for user %q.", u.Name).
			WithStackTrace(1).
			WithCause(endErr).
			WithRequestDump(r).
			Respond(w)
	}
}

// signoutPossible checks whether a signout is possible or not.
// If not an error is returned else the currently singed in user
// is returned.
func (l *signout) signoutPossible(r *http.Request) (*user.User, error) {
	u, getErr := l.session.GetCurrent(r)
	if getErr != nil {
		return nil, errors.ConstructStd(http.StatusInternalServerError,
			failedSignOut, unableToCheckSession, nil).
			WithLogf("Unable to get currently signed in user!").
			WithStackTrace(1).
			WithCause(getErr).
			WithRequestDump(r).
			Finish()
	}
	if u == nil {
		return nil, errors.ConstructStd(http.StatusBadRequest,
			failedSignOut, noSignedInUser, nil).
			WithLogf("Tried to sign out while no user is signed in!").
			WithStackTrace(1).
			WithRequestDump(r).
			Finish()
	}
	return u, nil
}
