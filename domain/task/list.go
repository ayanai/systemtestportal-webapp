/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package task

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
)

// List is a collection of task-assignments
type List struct {
	Username string // The "Name" of the user this task-list belongs to
	Tasks    []Item
}

// New List creates a new task-list for the user with the
// given name. The list contains no task-items.
//
// Returns the created list.
func NewList(username string) *List {
	return &List{
		Username: username,
		Tasks:    []Item{},
	}
}

// AddItem creates a new -.task item and adds it to the list.
// The amount of task-items is the id of the newly created item.
//
// Returns the list
func (l *List) AddItem(assignee, author id.ActorID, projectID id.ProjectID, taskType Type, refType ReferenceType, refID string, version, variant string, deadline time.Time) *List {
	item := Item{
		Index:     l.getMaxIndex() + 1,
		Assignee:  assignee,
		Author:    author,
		ProjectID: projectID,
		Type:      taskType,
		Reference: itemReference{
			Type: refType,
			ID:   refID,
		},
		Version:      version,
		Variant:      variant,
		Deadline:     deadline,
		CreationDate: time.Now().UTC().Round(time.Second),
		Done:         false,
	}
	l.Tasks = append(l.Tasks, item)
	return l
}

// getMaxIndex returns the maximum index of the task items.
// It is needed to compute the index of a new item.
// Taking the length of task items can lead to problems
// if any item was delete from the database.
//
// Returns -1 if there is no item in the list
func (l *List) getMaxIndex() int {
	maxIndex := -1
	for _, taskItem := range l.Tasks {
		if taskItem.Index > maxIndex {
			maxIndex = taskItem.Index
		}
	}
	return maxIndex
}

// SetTasksToDone sets all tasks that match the parameters to done (e.g. when unassigning from an assignment)
func (l *List) SetTasksToDone(taskType Type, refType ReferenceType, refID string, version string, variant string) *List {
	for i, task := range l.Tasks {
		if (task.Type == taskType && task.Reference == itemReference{Type: refType, ID: refID} && !task.Done && task.Version == version && task.Variant == variant) {
			l.Tasks[i].SetDone()
		}
	}
	return l
}

/*
GetTaskDone returns all items which are already done.
*/
func (l *List) GetTaskDone() []Item {
	done := make([]Item, 0)
	for _, item := range l.Tasks {
		if item.Done {
			done = append(done, item)
		}
	}
	return done
}

/*
GetTaskUndone returns all items which are not already done.
*/
func (l *List) GetTaskUndone() []Item {
	undone := make([]Item, 0)
	for _, item := range l.Tasks {
		if !item.Done {
			undone = append(undone, item)
		}
	}
	return undone
}
